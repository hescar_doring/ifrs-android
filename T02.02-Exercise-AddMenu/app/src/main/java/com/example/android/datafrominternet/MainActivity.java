/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.datafrominternet;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText mSearchBoxEditText;

    private TextView mUrlDisplayTextView;

    private TextView mSearchResultsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSearchBoxEditText = (EditText) findViewById(R.id.et_search_box);

        mUrlDisplayTextView = (TextView) findViewById(R.id.tv_url_display);
        mSearchResultsTextView = (TextView) findViewById(R.id.tv_github_search_results_json);
    }

    // Do 2 - 7 in menu.xml ///////////////////////////////////////////////////////////////////////
    // TODO (2) Crie um menu chamado 'main.xml' na pasta res->menu
    // TODO (3) Adicione um item ao menu
    // TODO (4) Configure o id do item para '@+id/action_search'
    // TODO (5) Configure a 'orderInCategory' para 1
    // TODO (6) Configure para exibir o item se existir espaço (use app:showAsAction)
    // TODO (7) Configure o títlo para o valor da string 'search' do arquivo 'strings.xml'
    // Do 2 - 7 in menu.xml ///////////////////////////////////////////////////////////////////////


    // TODO (8) Sobreescreva o método onCreateOptionsMenu
    // TODO (9) Dentro do método, use getMenuInflater().inflate para inflar o menu
    // TODO (10) Retorne 'true' para exibir o menu

    // TODO (11) Sobreescreva o método onOptionsItemSelected
    // TODO (12) Dentro do método, obtenha o ID do item que foi selecionado
    // TODO (13) Se o ID for 'R.id.action_search', exiba um Toast e retorne 'true' para indicar ao Android que o evento foi tratado
    // TODO (14) Chame '.show()' para exibir o Toast
    // TODO (15) Se o ID não for R.id.action_search', retorne 'super.onOptionsItemSelected'
}
